Bienvenue sur la documentation d'Open SNDS
=====================================

```{eval-rst}
.. toctree::
   :maxdepth: 2
   :caption: Contents:
```

# À propos du projet

Open SNDS est une application web pour interroger et visualiser les données ouvertes en santé.

Elle se concentre pour l'instant sur les [données ouvertes de l'Asssurance Maladie](https://assurance-maladie.ameli.fr/etudes-et-donnees) sur le SNDS, qui sont des données agrégées permettant d'obtenir un nombre de bénéfiaires ou un montant de remboursement pour un médicament donné.

L'application est accessible à cette addresse : [https://open-snds.has-sante.fr/](https://open-snds.has-sante.fr/).

Le code de ce projet est [sous licence EUPL](LICENCE).

![capture](_static/images/capture.png)

# Installation

## Pré-requis

- podman
- podman-compose
- python 3.8
- poetry

## Sources de données

La description des sources de données utilisées dans l'application sont disponibles à l'url : [https://open-snds.has-sante.fr/dataapp](https://open-snds.has-sante.fr/dataapp)
Les données utilisées par l'application peuvent être stockées soit en local soit sur un storage object MinIO. Les données sont ensuite lues et chargés en mémoire comme dataframe pandas.

## Configuration du repository

### Récupération du code

Récupération du code depuis gitlab :

    git clone https://gitlab.com/has-sante/public/open_snds.git
    cd open_snds

### Configuration des variables d'environment

Copie du template vers `.env`:

    cp template.env .env  # Crée un fichier de configuration

Remplacer les variables dans le `.env`.