# build me using following command from the root :
# podman build -t 'opensnds:latest' .

FROM python:3.8.10-slim

# Optionnal dependencies
ARG DOCS_DEPENDENCIES
# Poetry
ARG POETRY_VERSION="1.1.6"
# Setup Poetry for Python package and dependency management
ENV POETRY_HOME=/opt/poetry \
    POETRY_VIRTUALENVS_CREATE=true
ENV PATH="${POETRY_HOME}/bin:${PATH}"
# Install `poetry` via `pip` and system `python`
RUN pip install poetry==${POETRY_VERSION} && \
    poetry --version && \
    poetry config --list

# Pyspark
WORKDIR /opt/
RUN rm -rf /var/lib/apt/lists/*

RUN ls

RUN mkdir -p /opt/open_snds/opensnds/
RUN mkdir -p /opt/open_snds/opensnds/docs/
WORKDIR /opt/open_snds

RUN mkdir data-cache

COPY pyproject.toml poetry.lock ./

RUN poetry install --no-dev --no-interaction --no-root && rm -rf ~/.cache/pip && rm -rf ~/.cache/pypoetry/artifacts && rm -rf ~/.cache/pypoetry/cache

COPY ./opensnds ./opensnds

COPY ./docs ./docs

RUN ls -al

RUN poetry install --no-interaction --no-dev

RUN if [ "$DOCS_DEPENDENCIES" = "true" ]; then poetry install --no-dev --no-interaction --no-root --extras "docs" ;fi