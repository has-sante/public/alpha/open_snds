import json
import os
from pathlib import Path

from dotenv import load_dotenv
import fsspec
import pandas as pd


load_dotenv()

MINIO_KEY = os.getenv("MINIO_KEY")
MINIO_SECRET_KEY = os.getenv("MINIO_SECRET_KEY")
MINIO_URL = os.getenv("MINIO_URL")
PATH2STORAGE = os.getenv("PATH2STORAGE")
STORAGE_TYPE = os.getenv("STORAGE_TYPE")
STORAGE_CACHE_PATH = os.getenv("STORAGE_CACHE_PATH", "data-cache")
DATA_DIR = PATH2STORAGE

if STORAGE_TYPE == "file":
    STORAGE_OPTIONS = None
    FS = fsspec.filesystem("file")
elif STORAGE_TYPE == "s3":
    FS_STORAGE_OPTIONS = {
        "key": MINIO_KEY,
        "secret": MINIO_SECRET_KEY,
        "client_kwargs": {"endpoint_url": MINIO_URL},
    }
    STORAGE_OPTIONS = {}
    FS = fsspec.filesystem(
        "filecache",
        target_protocol="s3",
        target_options=FS_STORAGE_OPTIONS,
        cache_storage=STORAGE_CACHE_PATH,
    )
    STORAGE_OPTIONS["s3"] = FS_STORAGE_OPTIONS
    STORAGE_OPTIONS["filecache"] = {"cache_storage": STORAGE_CACHE_PATH}
else:
    raise EnvironmentError(f"{STORAGE_TYPE} : système de fichier inconnu")

ROOT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..")
DIR2RAW = os.path.join(DATA_DIR, "raw")
DIR2INTERIM = os.path.join(DATA_DIR, "interim")
DIR2CLEAN = os.path.join(DATA_DIR, "clean")

DIR2RAW_OPENDAMIR = os.path.join(DIR2RAW, "opendamir")
DIR2RAW_OPENMEDIC = os.path.join(DIR2RAW, "openmedic")
DIR2RAW_OPENBIO = os.path.join(DIR2RAW, "openbio")
DIR2RAW_OPENLPP = os.path.join(DIR2RAW, "openlpp")

DIR2INTERIM_OPENDAMIR = os.path.join(DIR2INTERIM, "opendamir.parquet")
DIR2INTERIM_OPENMEDIC = os.path.join(DIR2INTERIM, "openmedic.parquet")
DIR2INTERIM_OPENBIO = os.path.join(DIR2INTERIM, "openbio.parquet")
DIR2INTERIM_OPENLPP = os.path.join(DIR2INTERIM, "openlpp.parquet")

# On ne veut pas copier opendamir qui est une grosse db, pour laquelle nous avons deja le schema et nous ne faisons pas de preprocessing
DIR2CLEAN_OPENDAMIR = DIR2INTERIM_OPENDAMIR
DIR2CLEAN_OPENMEDIC = os.path.join(DIR2CLEAN, "openmedic.parquet")
DIR2CLEAN_OPENBIO = os.path.join(DIR2CLEAN, "openbio.parquet")
DIR2CLEAN_OPENLPP = os.path.join(DIR2CLEAN, "openlpp.parquet")


DIR2SCHEMAS = os.path.join(DATA_DIR, "schemas")
DIR2NOMENCLATURES = os.path.join(DATA_DIR, "nomenclatures")

DIR2REF = "referentiels"
DIR2LOGS = os.path.join("rapports", "logs")
DIR2ASSETS = os.path.join(ROOT_DIR, "opensnds/app/assets")
DIR2DATA_INFORMATION = os.path.join(DIR2ASSETS, "data_information")
PATH2POP_REGION = os.path.join(ROOT_DIR, DIR2REF, "pop_reg_sexe_1975_2020_fr.xls")

SUPPORTED_EXTENSIONS = [".csv$", ".CSV$", ".csv.gz$", ".CSV.gz$", ".zip"]

# Variables GITLAB utilisées pour requeter le dépot du HDH (nomenclatures et schémas)
GITLAB_COM_API_V4 = "https://gitlab.com/api/v4"
SCHEMA_SNDS_GITLAB_ID = "11935694"
SCHEMA_SNDS_GITLAB_RAW_URL = (
    "https://gitlab.com/healthdatahub/schema-snds/-/raw/master/"
)


# test
DIR2TEST_DATA = os.path.join(ROOT_DIR, "tests", "data")
DIR2RAW_TEST_DS = os.path.join(DIR2TEST_DATA, "raw", "test_dataset")
DIR2TEST_OPENDAMIR = os.path.join(DIR2TEST_DATA, "interim", "opendamir.parquet")
DIR2TEST_OPENMEDIC = os.path.join(DIR2TEST_DATA, "interim", "openmedic.parquet")


# interseting columns for databases
# opendamir
BEN_SEX_COD = "BEN_SEX_COD"
AGE_BEN_SNDS = "AGE_BEN_SNDS"
BEN_QLT_COD = "BEN_QLT_COD"
BEN_RES_REG = "BEN_RES_REG"
DRG_AFF_NAT = "DRG_AFF_NAT"
BEN_CMU_TOP = "BEN_CMU_TOP"

COLS_BENEF = [BEN_SEX_COD, AGE_BEN_SNDS, BEN_RES_REG, DRG_AFF_NAT, BEN_CMU_TOP]

PRS_NAT = "PRS_NAT"
ASU_NAT = "ASU_NAT"
COLS_PRESTA = [PRS_NAT, ASU_NAT, "ATT_NAT", "CPT_EN_TYP", "CPL_COD", "PRS_PPU_SEC"]

PSE_ACT_SNDS = "PSE_ACT_SNDS"
PSE_ACT_CAT = "PSE_ACT_CAT"
PSE_SPE_SNDS = "PSE_SPE_SNDS"
PSE_STJ_SNDS = "PSE_STJ_SNDS"
COLS_EXEC = [PSE_ACT_SNDS, PSE_ACT_CAT, PSE_SPE_SNDS, PSE_STJ_SNDS]

PSP_ACT_SNDS = "PSP_ACT_SNDS"
PSP_ACT_CAT = "PSP_ACT_CAT"
PSP_SPE_SNDS = "PSP_SPE_SNDS"
PSP_STJ_SNDS = "PSP_STJ_SNDS"
COLS_PRESC = [PSP_ACT_SNDS, PSP_ACT_CAT, PSP_SPE_SNDS, PSP_STJ_SNDS]

SOI_ANN = "SOI_ANN"
SOI_MOI = "SOI_MOI"
COLS_TEMP = [SOI_ANN, SOI_MOI]

COLS_OPENDAMIR = [*COLS_TEMP, *COLS_PRESTA, *COLS_BENEF, *COLS_EXEC, *COLS_PRESC]

YEAR = "year"
# Default output columns from the query_functions (manipulated by all outputs, ex : plots, and tables)
DNB = "Dénombrement"
NB_CONSOMMANTS = "Nb consommants"
NB_ACTES = "Nb actes"
NB_PATIENTS = "Nb patients"
REM = "Montant remboursé"
VALUE_COLS = [DNB, REM, NB_CONSOMMANTS]
DNB_LABEL = "Consommations"
NB_CONSOMMANTS_LABEL = "Consommants"
NB_CONSOMMANTS_LABEL_BORNE_HAUTE = "Consommants - ⚠ borne haute"
REM_LABEL = "Remboursements (€)"
PARQUET_TO_DATAFRAME_ENGINE = "koalas"
DISPLAY_RAPPORT = False


def load_nomenclatures():
    nomenclatures = {}
    for nomenclature_file in FS.ls(DIR2NOMENCLATURES):
        file_path = Path(nomenclature_file)
        if file_path.name.endswith("json"):
            with FS.open(nomenclature_file, "r") as fp:
                nomenclature_json = json.load(fp)
            primary_key = nomenclature_json["primaryKey"]
            if not isinstance(primary_key, list):
                primary_key = [primary_key]
            nomenclatures.setdefault(file_path.stem, {})["primary_key"] = primary_key
        else:
            try:
                nomenclature_df = pd.read_csv(FS.open(nomenclature_file))
            except:
                nomenclature_df = pd.read_csv(
                    FS.open(nomenclature_file),
                    sep=";",
                )
            nomenclatures.setdefault(file_path.stem, {})["data"] = nomenclature_df
    return nomenclatures


NOMENCLATURES = load_nomenclatures()
