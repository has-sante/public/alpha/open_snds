import shutil
from typing import Dict, Union

from fsspec.registry import filesystem
from opensnds.constants import (
    DIR2CLEAN,
    DIR2INTERIM,
    DIR2LOGS,
    DIR2NOMENCLATURES,
    DIR2SCHEMAS,
    FS,
    DATA_DIR,
)
from opensnds.config import SUPPORTED_DBS
import pandas as pd
import logging
import os
import re
from tableschema import Table
from opensnds.utils import sc
import argparse


def dataset_preprocessing(dbname: str, clean_db=False, nrows: int = 100000):
    logging.warning(f"Preprocess {dbname}")
    assert (
        dbname in SUPPORTED_DBS.keys()
    ), f"La base n'est pas supportée. les datasets supportés sont {SUPPORTED_DBS.keys()}"
    # lit uniquement les premieres lignes des grands datasets
    path2ds = os.path.join(DIR2INTERIM, f"{dbname}.parquet")
    if dbname in ["opendamir"]:
        df = sc.read.parquet(path2ds).limit(nrows).toPandas()
    else:
        # cherche et retire les variables de libelles (uniquement petits datasets)
        df = pd.read_parquet(path2ds, engine="pyarrow")
        for col in df.columns:
            reg = re.search("^L_(.*)|^l_(.*)", col)
            if reg is not None:
                label_col = reg.group(0)
                code_col = [catch for catch in reg.groups() if catch is not None][0]
                # les colonnes ne sont pas bien normalisees entre le label et la colonne source
                if code_col not in df.columns:
                    code_col = code_col.lower()
                    if code_col == "cip13":
                        code_col = "CIP13"
                    if code_col == "sexe":
                        code_col = "SEXE"
                create_nomenclature(df, code_col, label_col, dbname)
                df.drop([label_col], axis=1, inplace=True)
        df["year"] = df["year"].astype(int)
    # creation du schema
    _ = create_schema(df[:nrows], dbname)
    # TODO: better partitionning here could be possibler

    if dbname not in ["opendamir"]:
        path2clean_db = os.path.join(DIR2CLEAN, f"{dbname}.parquet")
        if clean_db and os.path.isdir(path2clean_db):
            shutil.rmtree(path2clean_db)
            logging.warning(f"Removing directory at {path2clean_db}")
        # os.makedirs(path2clean_db, exist_ok=True)
        # df.to_parquet(path2clean_db, partition_cols="year")
        df.to_parquet(
            DATA_DIR + "/clean/" + dbname + ".parquet",
            filesystem=FS,
        )


def create_nomenclature(
    df: pd.DataFrame,
    code_col: str,
    label_col: str,
    ds_name: str,
    force_creation: bool = False,
):
    nomenclature_name = f"{ds_name}_{code_col}"
    nomenclature_path = os.path.join(DIR2NOMENCLATURES, nomenclature_name + ".csv")
    if os.path.isfile(nomenclature_path):
        logging.warning(f"La nomenclature {nomenclature_name} existe déjà !")
        if force_creation:
            pass
        else:
            return
    else:
        logging.info(f"Creation d'une nouvelle nomenclature {nomenclature_name}")
    nomenclature = df[[code_col, label_col]].drop_duplicates()
    nomenclature.rename(columns={label_col: "LIB_" + code_col}, inplace=True)
    nomenclature.to_csv(nomenclature_path, index=False)
    # logic to create schema
    nomenclature_table = Table(nomenclature_path)
    nomenclature_table.read()
    nomenclature_table.infer()
    nomenclature_schema = nomenclature_table.schema
    nomenclature_schema.descriptor["name"] = nomenclature_name
    nomenclature_schema.descriptor["title"] = label_col
    nomenclature_schema.descriptor["primaryKey"] = code_col
    nomenclature_schema.commit()
    nomenclature_schema.save(
        os.path.join(DIR2NOMENCLATURES, nomenclature_name + ".json")
    )
    return nomenclature_schema.descriptor


def create_schema(
    df: pd.DataFrame, dfname: str, force_creation=False
) -> Dict[str, Union[Dict, str]]:
    logging.info(f"Creation du schema pour la table {dfname}")
    # check if not exist
    schema_path = os.path.join(DIR2SCHEMAS, dfname + ".json")
    if os.path.isfile(schema_path):
        logging.warning(f"Le schema {schema_path} existe déjà !")
        if force_creation:
            pass
        else:
            return
    # necessaire de sauvegarder la table pour Table
    path2tmp = os.path.join(DIR2INTERIM, "tmp_" + dfname + ".csv")
    df.to_csv(path2tmp, index=False)
    table = Table(path2tmp)
    table.read()
    table.infer()
    table_schema = table.schema
    for field in table_schema.fields:
        field.descriptor["description"] = ""
        field.descriptor["length"] = ""
        field.descriptor["nomenclature"] = "-"
        field.descriptor["dateCreated"] = ""
        field.descriptor["dateDeleted"] = ""
        field.descriptor["dateMissing"] = ""
        field.descriptor["observation"] = ""
        field.descriptor["regle_gestion"] = ""
        table_schema.update_field(field.name, field.descriptor)
    table_schema.commit()
    table_schema.save(schema_path)
    os.remove(path2tmp)
    return table_schema.descriptor


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--dbnames", nargs="+", default=["openmedic", "openbio", "openlpp"])
    ap.add_argument("-clean_db", action="store_true", default=False)
    args = vars(ap.parse_args())
    os.makedirs(DIR2LOGS, exist_ok=True)
    logging.basicConfig(
        filename=os.path.join(
            DIR2LOGS, "_".join(args["dbnames"]) + "__preprocessing.log"
        ),
        level=logging.WARNING,
    )
    logging.info(args)
    for dbname in args["dbnames"]:
        dataset_preprocessing(dbname, clean_db=args["clean_db"])
