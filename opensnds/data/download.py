import zipfile
from io import StringIO
import asyncio
import os
import requests

from pyppeteer import launch

from opensnds.constants import DATA_DIR


async def donwload_amelie():
    browser = await launch()
    page = await browser.newPage()
    url = "http://open-data-assurance-maladie.ameli.fr/LPP/download_file.php?file=Open_LPP_Bases_Complementaires/2016_SC1/NB_2016_sc1_age_sexe_spe.CSV.gz"
    await page.goto(url)
    await page.type("textarea", "HAS")
    # await page.
    await page.click("button")
    await page.screenshot({"path": "example.png"})
    await browser.close()


async def download_datagouv():
    browser = await launch()
    page = await browser.newPage()
    # await page._client.send('Page.setDownloadBehavior', params={"behavior": 'allow', "downloadPath": str(DATA_DIR / "hello.csv")})
    # url = "https://www.scansante.fr/sites/www.scansante.fr/files/content/470/open_ccam_18_.zip"
    # await page.goto(url)
    await page.screenshot({"path": os.path.join(str(DATA_DIR / "example.png"))})
    await browser.close()


# asyncio.get_event_loop().run_until_complete(download_datagouv())
print("hello")
headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
    "Connection": "keep-alive",
    "Content-Length": "514",
    "Content-Type": "application/x-www-form-urlencoded",
    "Cookie": "xtvrn=$311077$",
    "DNT": "1",
    "Host": "open-data-assurance-maladie.ameli.fr",
    "Origin": "http://open-data-assurance-maladie.ameli.fr",
    "Referer": "http://open-data-assurance-maladie.ameli.fr/LPP/download_file.php?file=Open_LPP_Bases_Complementaires/2017_SC1/NB_2017_sc1_age_sexe_reg_spe.CSV.gz",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0",
}
r = requests.post(
    url="http://open-data-assurance-maladie.ameli.fr/LPP/download_file2.php?file=Open_LPP_Bases_Complementaires/2017_SC1/NB_2017_sc1_age_sexe_reg_spe.CSV.gz",
    params={
        "message": "de",
        "recaptcha_response": "03AGdBq279yZRApoZpqscuIbVd7TbRbzTCvQURQnDwAyOkphUR2NCeec5zvDY_M6vbJ-o3npmIvX-bEqB4Fid7OX9Sn6rhG7dXcYiNHrjWtYzn3UT3FOlVDHXE48EdNH01gOhhcgfht-j4FMjxi6cXGrYActAhfZIuUrTRY-0tBbEsJGMolgVXSuz3rcNtcfvU-2R8QYNWheyvr786xegIPNl3LwEAtjbi1AZ40ton9ayS4fnAXttMiwU8Hs_-GwtDk8x1eNFxeILlJr604qIABgyhj0s7dRH6wkHmSriikKSJV6I-Kw5iXGPrZ-_mJYbjSz8FXyZ_IQq-fyDpwWadK7nyCoRPBHq7vtcvnd6dGUGow-hshY9CxKDO2qx2EEWJPzvtjOb39tPHgXdTElhA5rMaQygaVOLMKXJwjI-XgKfyAqtYTltnoX8JPIz4Qa48yhns_Q5fe2EmTY2Ec2KmTJ8uUQnH5wc3AQ",
    },
    headers=headers,  # cookies={"xtvrn": "$311077$"}
)
print(r.status_code)
print(r.headers)
print(r.content.decode())


# for zip, does not work with atih link (return a html)
def download_url(url, save_path, chunk_size=128):
    r = requests.get(url, stream=True)
    with open(save_path, "wb") as fd:
        for chunk in r.iter_content(chunk_size=chunk_size):
            fd.write(chunk)


zip_file_url = "https://www.scansante.fr/sites/www.scansante.fr/files/content/470/open_ccam_18_.zip"
# download_url(zip_file_url, str(DATA_DIR/"test.zip"))
