from pandas.core.window.rolling import Window
import dash_bootstrap_components as dbc
import dash_html_components as html

import dash_core_components as dcc
import dash_extensions as de

from opensnds.app.utils.front_utils import drawFigure
from opensnds.app.constants import (
    DEFAULT_PALETTE,
    default_level_choice,
    default_figure,
    default_year,
    default_ds_name,
    ds_name_choices,
)

# good layout to take inspration from https://stackoverflow.com/questions/63592900/plotly-dash-how-to-design-the-layout-using-dash-bootstrap-components


default_offset = 1
default_width = 10

layout = html.Div(
    children=[
        dbc.Row(
            [
                dbc.Col(
                    dbc.Alert(
                        html.P(
                            [
                                "Cette application expose les données ouvertes du ",
                                html.A(
                                    "portail open data de l'Assurance Maladie",
                                    href="https://assurance-maladie.ameli.fr/etudes-et-donnees/donnees/bases-de-donnees-open-data/liste-bases-de-donnees-open-data",
                                ),
                                ". La HAS n'est pas responsable de la qualité de ces données ou des possibles écarts avec les données individuelles du SNDS.",
                            ],
                            style={"margin-bottom": "0"},
                        ),
                        color="#034ECA",
                    )
                ),
            ],
            justify="start",
            style={"margin": 10},
            align="center",
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.Div(
                            html.Label("Jeux de données (Univers)"),
                            style={"font-weight": "bold"},
                        ),
                        html.Div(
                            dcc.Dropdown(
                                options=ds_name_choices,
                                id="input-dataset-name",
                                multi=False,
                                value=default_ds_name,
                            )
                        ),
                        dbc.Checklist(
                            options=[
                                {
                                    "label": "Afficher les requêtes SNDS",
                                    "value": "viewSndsQuery",
                                }
                            ],
                            value=[],
                            id="checklist-query-input",
                            style={"display": "none"},
                        ),
                    ]
                ),
                dbc.Col(
                    [
                        html.Div(
                            html.Label("Filtrer sur", style={"font-weight": "bold"})
                        ),
                        html.Div(
                            dcc.Dropdown(
                                # options= defined by callback
                                id="dashboard-level-choices",
                                multi=False,
                                value=default_level_choice,
                                optionHeight=60,
                                placeholder=f"ex : {default_level_choice}",
                            )
                        ),
                    ],
                    width={"size": 3},
                ),
                dbc.Col(
                    html.Div(
                        [
                            html.Label("En conservant", style={"font-weight": "bold"}),
                            dcc.Dropdown(
                                id="dashboard-filter-values",
                                multi=True,
                                value=None,
                                optionHeight=60,
                                placeholder="Rechercher un code",
                            ),
                            dbc.FormGroup(
                                [
                                    dbc.Label("Liste de codes"),
                                    dbc.Input(
                                        placeholder="Entrer une liste de codes séparés par des virgules ici...",
                                        type="text",
                                        id="dashboard-search-input-list-codes",
                                    ),
                                    # dbc.FormText("Type something in the box above"),
                                ],
                                style={"margin-top": "15px"},
                            ),
                        ],
                        # probes={
                        #     "probe": [
                        #         {"id": "dashboard-filter-values", "prop": "value"},
                        #         {"id": "dashboard-input-list-codes", "prop": "value"}
                        #     ]}, id="monitor"),
                    ),
                    width={"size": 5},
                ),
                dbc.Col(
                    html.Div(
                        [
                            dbc.Button("Go", color="info", id="dashboard-query-button"),
                        ]
                    ),
                    width=1,
                ),
            ],
            justify="start",
            style={"margin": 5},
            align="center",
        ),
        dbc.Row(
            [
                dbc.Col(html.Div(id="dashboard-warning")),
            ],
            justify="start",
            style={"margin": 10},
            align="center",
        ),
        dbc.Row(
            [
                dbc.Col(html.Div(id="dashboard-warning-value")),
            ],
            justify="start",
            style={"margin": 10},
            align="center",
        ),
        html.Hr(),
        dbc.Row(
            [
                dbc.Col(html.H4("Répartition par année"), width=3),
                dbc.Col(
                    html.Div(
                        [
                            dbc.Button(
                                "Voir la requête SNDS",
                                id="click-query-sql",
                                className="click-query-sql-button",
                                color="success",
                                style=dict(display="none"),
                            ),
                            dbc.Popover(
                                id="dashboard-clipboard-sql",
                                target="click-query-sql",
                                trigger="legacy",
                            ),
                        ]
                    ),
                ),
            ]
        ),
        html.Br(),
        dbc.Row(
            [
                dbc.Col(
                    [dbc.Spinner(drawFigure(default_figure, "fig-time"))],
                    width={"size": default_width, "offset": default_offset},
                    align="center",
                )
            ]
        ),
        html.Br(),
        html.Div(
            [
                dbc.Button(
                    "Exporter en fichier Excel",
                    id="export-year",
                    n_clicks=0,
                    color="success",
                ),
                de.Download(id="download-year"),
            ],
            style={"text-align": "center", "margin-bottom": "20px"},
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.Div(id="output-time-df"),
                    ],
                    width={"size": default_width, "offset": default_offset},
                    align="center",
                ),
            ]
        ),
        html.Hr(),
        dbc.Row(
            [
                dbc.Col(html.H4("Répartition par sexe")),
                dbc.Col(html.Div(html.Label("Année")), width=0.8),
                dbc.Col(
                    html.Div(
                        dcc.Dropdown(
                            # options= defined by callback
                            id="dashboard-year-choices-sex",
                            multi=False,
                            value=default_year,
                            optionHeight=20,
                        )
                    ),
                    width=2,
                ),
                dbc.Col(
                    html.Div(
                        [
                            dbc.Button(
                                "Voir la requête SNDS",
                                id="click-query-sql-sex",
                                className="click-query-sql",
                                color="success",
                                style=dict(display="none"),
                            ),
                            dbc.Popover(
                                id="dashboard-clipboard-sql-sex",
                                target="click-query-sql-sex",
                                trigger="legacy",
                            ),
                        ]
                    ),
                    width={"size": 2},
                ),
            ],
            justify="start",
            style={"padding": 5},
            align="center",
        ),
        html.Br(),
        dbc.Row(
            [
                dbc.Col(
                    [dbc.Spinner(drawFigure(default_figure, "fig-sex"))],
                    width={"size": default_width, "offset": default_offset},
                    align="center",
                ),
            ]
        ),
        html.Br(),
        html.Div(
            [
                dbc.Button(
                    "Exporter en fichier Excel",
                    id="export-sex",
                    n_clicks=0,
                    color="success",
                ),
                de.Download(id="download-sex"),
            ],
            style={"text-align": "center", "margin-bottom": "20px"},
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.Div(id="output-sex-df"),
                    ],
                    width={"size": default_width, "offset": default_offset},
                    align="center",
                ),
            ]
        ),
        html.Hr(),
        dbc.Row(
            [
                dbc.Col(html.H4("Répartition par classe d'âge"), width=3),
                dbc.Col(html.Div(html.Label("Année")), width=0.8),
                dbc.Col(
                    html.Div(
                        dcc.Dropdown(
                            # options= defined by callback
                            id="dashboard-year-choices-age",
                            multi=False,
                            value=default_year,
                            optionHeight=20,
                        )
                    ),
                    style={"float": "right"},
                    width=2,
                ),
                dbc.Col(
                    html.Div(
                        [
                            dbc.Button(
                                "Voir la requête SNDS",
                                id="click-query-sql-age",
                                className="click-query-sql",
                                color="success",
                                style=dict(display="none"),
                            ),
                            dbc.Popover(
                                id="dashboard-clipboard-sql-age",
                                target="click-query-sql-age",
                                trigger="legacy",
                            ),
                        ]
                    ),
                    width={"size": 8},
                ),
            ],
            justify="start",
            style={"padding": 5},
            align="center",
        ),
        html.Br(),
        dbc.Row(
            [
                dbc.Col(
                    [dbc.Spinner(drawFigure(default_figure, "fig-age"))],
                    width={"size": default_width, "offset": default_offset},
                    align="center",
                )
            ]
        ),
        html.Br(),
        html.Div(
            [
                dbc.Button(
                    "Exporter en fichier Excel",
                    id="export-age",
                    n_clicks=0,
                    color="success",
                ),
                de.Download(id="download-age"),
            ],
            style={"text-align": "center", "margin-bottom": "20px"},
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.Div(id="output-age-df"),
                    ],
                    width={"size": default_width, "offset": default_offset},
                )
            ],
            justify="start",
        ),
        html.Br(),
        dbc.Row(
            [
                dbc.Col(html.H4("Répartition par région"), width=3),
                dbc.Col(html.Div(html.Label("Année")), width=0.8),
                dbc.Col(
                    html.Div(
                        dcc.Dropdown(
                            # options= defined by callback
                            id="dashboard-year-choices-reg",
                            multi=False,
                            value=default_year,
                            optionHeight=20,
                        )
                    ),
                    style={"float": "right"},
                    width=2,
                ),
                dbc.Col(
                    html.Div(
                        [
                            dbc.Button(
                                "Voir la requête SNDS",
                                id="click-query-sql-reg",
                                className="click-query-sql",
                                color="success",
                                style=dict(display="none"),
                            ),
                            dbc.Popover(
                                id="dashboard-clipboard-sql-reg",
                                target="click-query-sql-reg",
                                trigger="legacy",
                            ),
                        ]
                    ),
                    width={"size": 8},
                ),
            ],
            justify="start",
            style={"padding": 5},
            align="center",
        ),
        html.Br(),
        dbc.Row(
            [
                dbc.Col(
                    [dbc.Spinner(drawFigure(default_figure, "fig-reg"))],
                    width={"size": default_width, "offset": default_offset},
                ),
            ]
        ),
        html.Br(),
        html.Div(
            [
                dbc.Button(
                    "Exporter en fichier Excel",
                    id="export-reg",
                    n_clicks=0,
                    color="success",
                ),
                de.Download(id="download-reg"),
            ],
            style={"text-align": "center", "margin-bottom": "20px"},
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.Div(id="output-reg-df"),
                    ],
                    width={"size": default_width, "offset": default_offset},
                )
            ],
            justify="start",
            style={"padding": 0},
        ),
        html.Br(),
        dbc.Row(
            [
                dbc.Col(html.H4("Répartition par spécialité du prescripteur"), width=3),
                dbc.Col(html.Div(html.Label("Année")), width=0.8),
                dbc.Col(
                    html.Div(
                        dcc.Dropdown(
                            # options= defined by callback
                            id="dashboard-year-choices-speprec",
                            multi=False,
                            value=default_year,
                            optionHeight=20,
                        )
                    ),
                    style={"float": "right"},
                    width=2,
                ),
            ]
        ),
        html.Br(),
        dbc.Row(
            [
                dbc.Col(
                    [dbc.Spinner(drawFigure(default_figure, "fig-speprec"))],
                    width={"size": default_width, "offset": default_offset},
                ),
            ]
        ),
        html.Br(),
        html.Div(
            [
                dbc.Button(
                    "Exporter en fichier Excel",
                    id="export-speprec",
                    n_clicks=0,
                    color="success",
                ),
                de.Download(id="download-speprec"),
            ],
            style={"text-align": "center", "margin-bottom": "20px"},
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.Div(id="output-speprec-df"),
                    ],
                    width={"size": default_width, "offset": default_offset},
                )
            ],
            justify="start",
            style={"padding": 0},
        ),
    ]
)
