import dash_bootstrap_components as dbc
import dash_html_components as html

import dash_bootstrap_components as dbc
import dash_core_components as dcc
from dash_html_components.H4 import H4
from dash_html_components.P import P

from opensnds.app.constants import home_page_md, encoded_image


layout = html.Div(
    children=[
        html.H4("Contacts"),
        html.P(
            [
                "Cette application est développée par la mission data de la Haute Autorité de Santé. Nous contacter : ",
                html.A("data@has-sante.fr", href="mailto:data@has-sante.fr"),
            ]
        ),
        html.P(
            [
                "🦊 Le code source du projet est disponible sur le ",
                html.A(
                    "gitlab de la HAS.",
                    href="https://gitlab.com/has-sante/public/alpha/open_snds",
                ),
            ]
        ),
        html.H4("Remerciements"),
        html.P(
            [
                "Cette application expose les données ouvertes par l'",
                html.A(
                    "Assurance Maladie",
                    href="https://assurance-maladie.ameli.fr/etudes-et-donnees/donnees/bases-de-donnees-open-data/liste-bases-de-donnees-open-data#text_133947",
                ),
                ", que nous remercions vivement pour cette production continue depuis 2014.",
            ]
        ),
    ]
)
