import dash_bootstrap_components as dbc
import dash_html_components as html
import dash
import dash_core_components as dcc

app = dash.Dash()

layout = html.Div(
    children=[
        html.H4("Guide d'utilisation de l'application Open SNDS"),
        html.P(
            [
                "L'application permet de visualiser les données agrégées du    ",
                html.A(
                    "SNDS",
                    href="https://documentation-snds.health-data-hub.fr/introduction/01-snds.html",
                ),
                " publiées en open data.",
            ]
        ),
        dbc.Row(
            children=[
                dbc.Col(
                    children=[
                        html.H5("Pour utiliser l'application Open SNDS"),
                        html.Ol(
                            [
                                html.Li(
                                    [
                                        "Choisir un jeu de données (ex : openmedic)",
                                    ]
                                ),
                                html.Li(
                                    ["Choisir un niveau de filtre (ex : ATC1 ou ATC5)"]
                                ),
                                html.Li(
                                    [
                                        "Sélectionner la liste des codes à filtrer, en utilisant le moteur de recherche ou en collant une liste de codes"
                                    ]
                                ),
                                html.Li(["Visualiser la répartition par année"]),
                                html.Li(
                                    [
                                        "Sélectionner une année et visualiser la répartition par sexe, classe d'âge et région"
                                    ]
                                ),
                                html.Li(
                                    [
                                        'Exporter les images d\'intérêt au format png (bouton "appareil photo" sur le menu de l\'image), ou les tableaux statistiques d\'intérêt au format Excel (bouton "Exporter au-dessus du tableau")'
                                    ]
                                ),
                                html.Li(
                                    [
                                        'Visualiser la correspondance en requêtes SNDS après avoir coché la case "Afficher les requêtes SNDS" et en cliquant sur le bouton "Voir la requête SNDS"'
                                    ]
                                ),
                            ]
                        ),
                    ],
                    width=8,
                ),
            ],
            className="stepGuide",
        ),
        dbc.Row(
            dbc.Col(
                children=[
                    html.Img(
                        src=app.get_asset_url("CaptureOpenSnds.png"),
                        style={"width": "-webkit-fill-available"},
                    ),
                ],
                width=8,
            ),
        ),
    ]
)
