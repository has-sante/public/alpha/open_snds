import dash_bootstrap_components as dbc
import dash_html_components as html

import dash_bootstrap_components as dbc
import dash_core_components as dcc
from dash_html_components.H4 import H4
from dash_html_components.P import P

from opensnds.app.constants import home_page_md, encoded_image

layout = html.Div(
    children=[
        html.Div(
            children=[
                html.H4("Quelles données mobiliser pour quels usages ?"),
                html.P(
                    "Le SNDS permet de répondre à de nombreuses questions chiffrées dans le domaine sanitaire. Des sources de données dérivées (agrégées) sont disponibles, pour gagner du temps ou pour qui n'aurait pas accès aux données détaillées. La source pertinente à mobiliser varie selon le champ d'étude (ville, hôpital), la thématique considérée (médicaments, actes de biologie, actes médicaux, etc.), ainsi que le niveau de finesse nécessaire (infra-annuel, croisement de plusieurs codes pour un même consommant, etc.)."
                ),
                html.P(
                    "On trouve en open data des données à différents niveaux de granularité :"
                ),
                html.Ul(
                    [
                        html.Li(
                            [
                                "des séries mensuelles univariées : ",
                                html.A(
                                    " Medic'AM, Retroced'AM, LPP AM",
                                    href="https://assurance-maladie.ameli.fr/etudes-et-donnees/donnees/tableaux-statistiques/liste-complete-tableaux-statistiques#text_141888",
                                ),
                            ]
                        ),
                        html.Li(
                            [
                                "des données agrégées par année, en fonction des classes d'âge, sexes et régions : ",
                                html.A(
                                    " données ouvertes de la CNAM",
                                    href="https://assurance-maladie.ameli.fr/etudes-et-donnees/donnees/tableaux-statistiques/liste-complete-tableaux-statistiques#text_141888",
                                ),
                                ", notamment Open Medic et Open Bio exposées dans cette application.",
                            ]
                        ),
                    ]
                ),
                html.P("On trouve en fonction de l'univers et du champ d'étude :"),
                html.Ul(
                    [
                        html.Li(
                            "Pour les dispensations de médicaments en ville : Medic'AM, Open Medic & Open PHMEV"
                        ),
                        html.Li(
                            "Pour les dispensations de médicaments en hôpital : Retroced'AM"
                        ),
                        html.Li("Pour les actes de biologie en ville : Open Bio"),
                        html.Li("Pour les dispositifs médicaux en ville : Open LPP"),
                        html.Li(
                            "Pour les diagnostics à l'hôpital : ScanSanté, Diamant"
                        ),
                        html.Li("Pour les actes médicaux en hôpital : Open CCAM"),
                        html.Li("Pour les causes médicales de décès : CepiDC"),
                    ]
                ),
                html.P(
                    [
                        "Le tableau ci-dessous résume de façon partielle ce paysage complexe. Des filtres sont disponibles pour faciliter la navigation. ",
                        "Etalab a également réalisé un travail de recensement des bases et jeux de données publics dans le domaine de la santé, disponible à cette",
                        html.A(
                            " adresse",
                            href="https://www.data.gouv.fr/fr/datasets/inventaire-des-bases-de-donnees-relatives-a-la-sante/",
                        ),
                        ".",
                    ]
                ),
            ],
            style={"width": "80%"},
        ),
        dbc.Row(
            [
                dbc.Col(
                    html.Div(
                        html.Iframe(
                            src="https://airtable.com/embed/shrjfR2D53X82fC54?backgroundColor=red&viewControls=on",
                            width="100%",
                            height="780px",
                        )
                    ),
                    align="center",
                )
            ],
            align="center",
        ),
    ]
)
