import dash_bootstrap_components as dbc
import dash_html_components as html
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
from opensnds.constants import DIR2ASSETS, DISPLAY_RAPPORT

from opensnds.app.constants import home_page_md, encoded_image
import os

layout = html.Div()
if DISPLAY_RAPPORT:
    app = dash.Dash()

    text_markdown = ""
    with open(os.path.join(DIR2ASSETS, "rapport.md"), "r") as f:
        text_markdown = f.read()
    external_stylesheets = [os.path.join(DIR2ASSETS, "table_markdown.css")]

    layout = html.Div(
        children=[
            dcc.Markdown(text_markdown),
        ]
    )
