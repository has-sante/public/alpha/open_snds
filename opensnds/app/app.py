#!/usr/bin/env python

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.
# dash imports
import logging
import os
import datetime
from typing import List
from opensnds.constants import DISPLAY_RAPPORT
import plotly.graph_objects as go

import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html

from dash.dependencies import Input, Output, State
from dash_extensions.snippets import send_bytes

import pandas as pd
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

pd.options.mode.chained_assignment = "raise"

# app utils imports
from opensnds.config import SUPPORTED_DBS
from opensnds.app.utils.front_utils import nice_dash_table
from opensnds.app.utils.back_utils import (
    get_group_histogram,
    get_time_barplot,
    query_table,
    return_nomenclature_choices,
    tradSQL,
    encapsSAS,
)

from opensnds.app.constants import (
    DASHBOARD_PAGENAME,
    GUIDE_PAGENAME,
    DATAGUIDE_PAGENAME,
    RAPPORT_PAGENAME,
    DATAAPP_PAGENAME,
    ABOUT_PAGENAME,
    GUIDE_PAGENAME,
    DEBUG_APP,
    PAGE_IDS,
    PAGE_NAMES,
    SENTRY_DSN,
    REQUESTS_CA_BUNDLE,
    default_ds_name,
    datasets_years,
    default_year,
)
from opensnds.preprocessing.schema_handler import load_schemas

from opensnds.app.utils.back_utils import load_datasets

# app tabs import
import opensnds.app.tabs.dashboard_tab as dashboard_tab
import opensnds.app.tabs.data_tab as data_tab
import opensnds.app.tabs.opendata_tab as opendata_tab
import opensnds.app.tabs.rapport_tab as rapport_tab
import opensnds.app.tabs.about_tab as about_tab
import opensnds.app.tabs.userguide_tab as userguide_tab

schemas = load_schemas()

datasets = load_datasets(False)
logging.info(f"Open SNDS is running with datasets : {', '.join(datasets.keys())}")

if SENTRY_DSN:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        ca_certs=REQUESTS_CA_BUNDLE,
        integrations=[FlaskIntegration()],
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0,
        # By default the SDK will try to use the SENTRY_RELEASE
        # environment variable, or infer a git commit
        # SHA as release, however you may want to set
        # something more human-readable.
        # release="myapp@1.0.0",
    )

app = dash.Dash(
    __name__,
    # used to avoid exception on callbacks that apply to tabs element
    # another solution to dig is to put the app declaration in a separated file (server.py) as indicated here :
    # https://community.plotly.com/t/multi-page-dash-app-call-back-not-responding/9309/3
    suppress_callback_exceptions=True,
    external_stylesheets=[dbc.themes.FLATLY],
    # these meta_tags ensure content is scaled correctly on different devices
    # see: https://www.w3schools.com/css/css_rwd_viewport.asp for more
    meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}],
    update_title="Chargement d'Open SNDS",
)

server = app.server
# we use the Row and Col components to construct the sidebar header
# it consists of a title, and a toggle, the latter is hidden on large screens
# TODO: Cette sidebar serait plus pertinente en haut (permutation avec la sidebar de recherche)
popover_beta_children = [
    dbc.PopoverBody(
        [
            html.P(
                """
        L'application est en cours de développement, ne pas hésiter à faire remonter des anomalies.
        L'interprétation des données doit être prudente."""
            )
        ],
        style={"text-align": "justify"},
    )
]

sidebar_header = dbc.Row(
    [
        dbc.Col(
            [
                html.H4(
                    "Open SNDS",
                    style={
                        "color": "#149A80",
                        "padding-left": "7px",
                        "padding-right": "0px",
                        "padding-top": "20px",
                        "padding-bottom": "0px",
                    },
                    className="titleApp",
                ),
                html.P(
                    "Version Beta",
                    id="beta-opensnds",
                    style={
                        "background-color": "#FF6168",
                        "text-align": "center",
                        "font-weight": "bolder",
                        "color": "white",
                        "margin-left": "5px",
                        "margin-right": "5px",
                        "margin-bottom": "0",
                        "margin-left": "10px",
                        "margin-right": "8px",
                        "margin-bottom": "0",
                        "border-radius": "10px",
                    },
                ),
                dbc.Popover(
                    children=popover_beta_children,
                    id="popover-beta-opensnds",
                    target="beta-opensnds",
                    trigger="hover",
                ),
            ]
        ),
        dbc.Col(
            [
                html.Button(
                    # use the Bootstrap navbar-toggler classes to style
                    html.Span(className="navbar-toggler-icon"),
                    className="navbar-toggler",
                    # the navbar-toggler classes don't set color
                    style={
                        "color": "rgba(0,0,0,.5)",
                        "border-color": "rgba(0,0,0,.1)",
                    },
                    id="navbar-toggle",
                ),
                html.Button(
                    # use the Bootstrap navbar-toggler classes to style
                    html.Span(className="navbar-toggler-icon"),
                    className="navbar-toggler",
                    # the navbar-toggler classes don't set color
                    style={
                        "color": "rgba(0,0,0,.5)",
                        "border-color": "rgba(0,0,0,.1)",
                    },
                    id="sidebar-toggle",
                ),
            ],
            # the column containing the toggle will be only as wide as the
            # toggle, resulting in the toggle being right aligned
            width="auto",
            # vertically align the toggle in the center
            align="center",
        ),
    ]
)

sidebar = html.Div(
    [
        sidebar_header,
        # we wrap the horizontal rule and short blurb in a div that can be
        # hidden on a small screen
        html.Div(
            [
                dbc.Nav(
                    [
                        dbc.NavItem(
                            dbc.NavLink(
                                "Tableau de bord",
                                active=True,
                                id=DASHBOARD_PAGENAME + "-link",
                                href="/" + DASHBOARD_PAGENAME,
                            )
                        ),
                        dbc.NavItem(
                            dbc.NavLink(
                                "Guide d'utilisation",
                                active=False,
                                id=GUIDE_PAGENAME + "-link",
                                href="/" + GUIDE_PAGENAME,
                            )
                        ),
                        dbc.NavItem(
                            dbc.NavLink(
                                "Données disponibles",
                                active=False,
                                id=DATAAPP_PAGENAME + "-link",
                                href="/" + DATAAPP_PAGENAME,
                            )
                        ),
                        dbc.NavItem(
                            dbc.NavLink(
                                "Quelles données pour quels usages ?",
                                id=DATAGUIDE_PAGENAME + "-link",
                                active=False,
                                href="/" + DATAGUIDE_PAGENAME,
                            )
                        ),
                        dbc.NavItem(
                            dbc.NavLink(
                                "À propos",
                                active=False,
                                id=ABOUT_PAGENAME + "-link",
                                href="/" + ABOUT_PAGENAME,
                            )
                        ),
                    ],
                    vertical="md",
                    pills=True,
                )
            ],
            id="blurb",
            style={"margin-top": "20px"},
        ),
        html.Div(
            html.Img(src="./assets/HAS_Logo.svg", style={"width": "95%"}),
            style={
                "position": "absolute",
                "bottom": "0px",
                "margin-bottom": "20px",
                "margin-top": "20px",
                "margin-right": "10px",
            },
        )
        # use the Collapse component to animate hiding / revealing links
    ],
    id="sidebar",
)

content = html.Div(id="page-content")

app.layout = html.Div([dcc.Location(id="url"), sidebar, content])
app.title = "Open SNDS"

# this callback uses the current pathname to set the active state of the
# corresponding nav link to true, allowing users to tell see page they are on

@app.server.route("/ping")
def ping():
  return "{status: ok}"

@app.callback(
    [Output(page_id, "active") for page_id in PAGE_IDS],
    [Input("url", "pathname")],
)
def toggle_active_links(pathname):
    if pathname == "/":
        # Treat page 1 as the homepage / index
        return [True, False, False, False, False]
    return [pathname[1:] == p_name for p_name in PAGE_NAMES]


@app.callback(Output("page-content", "children"), [Input("url", "pathname")])
def render_page_content(pathname):
    if pathname in ["/"]:
        return dashboard_tab.layout
    elif pathname == "/" + DASHBOARD_PAGENAME:
        return dashboard_tab.layout
    elif pathname == "/" + GUIDE_PAGENAME:
        return userguide_tab.layout
    elif pathname == "/" + DATAGUIDE_PAGENAME:
        return data_tab.layout
    elif pathname == "/" + DATAAPP_PAGENAME:
        return opendata_tab.layout
    elif pathname == "/" + RAPPORT_PAGENAME and DISPLAY_RAPPORT:
        return rapport_tab.layout
    elif pathname == "/" + ABOUT_PAGENAME:
        return about_tab.layout
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ]
    )


@app.callback(
    Output("sidebar", "className"),
    [Input("sidebar-toggle", "n_clicks")],
    [State("sidebar", "className")],
)
def toggle_classname(n, classname):
    if n and classname == "":
        return "collapsed"
    return ""


@app.callback(
    Output("collapse", "is_open"),
    [Input("navbar-toggle", "n_clicks")],
    [State("collapse", "is_open")],
)
def toggle_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(
    Output("click-query-sql", component_property="style"),
    Output("click-query-sql-sex", component_property="style"),
    Output("click-query-sql-reg", component_property="style"),
    Output("click-query-sql-age", component_property="style"),
    [Input("checklist-query-input", "value")],
)
def display_button_query(dummy):
    if len(dummy) > 0:
        return [
            {"display": "inline-block"},
            {"display": "inline-block"},
            {"display": "inline-block"},
            {"display": "inline-block"},
        ]
    return [
        {"display": "none"},
        {"display": "none"},
        {"display": "none"},
        {"display": "none"},
    ]


# modal callbacks


def toggle_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


app.callback(
    Output("modal", "is_open"),
    [Input("open", "n_clicks"), Input("close", "n_clicks")],
    [State("modal", "is_open")],
)(toggle_modal)


# Callback for the query tab
@app.callback(
    Output("output-response-df", "children"),
    [Input("input-query-button", "n_clicks")],
    state=[
        State("input-group-cols", "value"),
        State("input-filter-col", "value"),
        State("filter-values-cols", "value"),
        State("input-dataset-name", "value"),
    ],
)
def return_query_response(n_clicks, group_cols, filter_col, filtered_values, ds_name):
    if ds_name is None:
        ds_name = default_ds_name
    if group_cols is None:
        response_df = pd.DataFrame(
            {"Instruction": 'Cliquer sur le boutton "Go" pour commencer'}
        )
    else:
        if filtered_values is not None:
            filtered_values = [str(item) for item in filtered_values]
        else:
            filtered_values = []
        response = query_table(
            ds_name,
            datasets[ds_name],
            schemas[ds_name],
            group_cols,
            filter_col=filter_col,
            filter_values=filtered_values,
        )
        response_df = response["table"]
    if filtered_values is None:
        display_label_nb_consommants = 0
    display_label_nb_consommants = len(filtered_values)
    return nice_dash_table(
        response_df,
        "dataset_response",
        None,
        display_label_nb_consommants,
    )


@app.callback(
    Output("filter-values-cols", "options"),
    [Input("input-filter-col", "value"), State("input-dataset-name", "value")],
)
def query_filter_nomenclature_choices(filter_col, ds_name):
    return return_nomenclature_choices(filter_col, ds_name, schemas)


@app.callback(
    Output("input-filter-col", "options"), [Input("input-dataset-name", "value")]
)
def return_filter_choice(ds_name):
    if ds_name is None:
        ds_name = default_ds_name
    filter_choices = [
        {
            "label": schemas[ds_name].get_field(col).descriptor["description"],
            "value": col,
        }
        for col in SUPPORTED_DBS[ds_name]["filter_cols"]
    ]
    return filter_choices


@app.callback(
    Output("input-group-cols", "options"), [Input("input-dataset-name", "value")]
)
def return_group_choice(ds_name):
    if ds_name is None:
        ds_name = default_ds_name
    group_choices = [
        {
            "label": schemas[ds_name].get_field(col).descriptor["description"],
            "value": col,
        }
        for col in SUPPORTED_DBS[ds_name]["group_cols"]
    ]
    return group_choices


@app.callback(
    [Output("dashboard-search-input-list-codes", "value")],
    [Input("dashboard-filter-values", "value")],
)
def test(filter_values):
    return [filter_values]


@app.callback(
    [
        Output("fig-sex", "figure"),
        Output("fig-age", "figure"),
        Output("fig-reg", "figure"),
        Output("fig-time", "figure"),
        Output("fig-speprec", "figure"),
        Output("output-sex-df", "children"),
        Output("output-age-df", "children"),
        Output("output-reg-df", "children"),
        Output("output-time-df", "children"),
        Output("output-speprec-df", "children"),
        Output("input-dataset-name", "value"),
        Output("dashboard-level-choices", "options"),
        Output("dashboard-filter-values", "options"),
        Output("dashboard-year-choices-sex", "options"),
        Output("dashboard-year-choices-age", "options"),
        Output("dashboard-year-choices-reg", "options"),
        Output("dashboard-year-choices-speprec", "options"),
        Output("dashboard-year-choices-sex", "value"),
        Output("dashboard-year-choices-age", "value"),
        Output("dashboard-year-choices-reg", "value"),
        Output("dashboard-year-choices-speprec", "value"),
        Output("dashboard-level-choices", "value"),
        Output("dashboard-filter-values", "value"),
        Output("dashboard-warning", "children"),
        Output("dashboard-warning-value", "children"),
    ],
    [
        Input("input-dataset-name", "value"),
        Input("dashboard-level-choices", "value"),
        Input("dashboard-query-button", "n_clicks"),
        Input("dashboard-year-choices-sex", "value"),
        Input("dashboard-year-choices-age", "value"),
        Input("dashboard-year-choices-reg", "value"),
        Input("dashboard-year-choices-speprec", "value"),
    ],
    state=[
        State("dashboard-search-input-list-codes", "value"),
    ],
)
def return_filter_choice(
    ds_name,
    filter_col,
    dashboard_query_button,
    years_sex,
    years_age,
    years_reg,
    years_speprec,
    filter_values,
):
    # Sycnhro champ années
    ctx = dash.callback_context
    if ctx.triggered[-1]["prop_id"] == "dashboard-year-choices-sex.value":
        years = years_sex
    elif ctx.triggered[-1]["prop_id"] == "dashboard-year-choices-age.value":
        years = years_age
    elif ctx.triggered[-1]["prop_id"] == "dashboard-year-choices-reg.value":
        years = years_reg
    elif ctx.triggered[-1]["prop_id"] == "dashboard-year-choices-speprec.value":
        years = years_speprec
    else:
        years = default_year
    ## Remise à zéro des codes si changement de niveau de filtres<s
    if ctx.triggered[-1]["prop_id"] == "dashboard-level-choices.value":
        filter_values = None
    """
    Update the drop down menus after selecting a a dataset and a level of choice
    """
    logging.info("UPDATE DASHBOARD")
    if ds_name == None:
        ds_name = default_ds_name
    level_choices = [
        {
            "label": f"{col} : "
            + schemas[ds_name].get_field(col).descriptor["description"].capitalize(),
            "value": col,
        }
        for col in SUPPORTED_DBS[ds_name]["level_choices"]
    ]
    years_choices = datasets_years[ds_name]
    ## Changement des codes pour filtres si changement du dataset
    if filter_col not in SUPPORTED_DBS[ds_name]["filter_cols"] or filter_col is None:
        filter_col = SUPPORTED_DBS[ds_name]["filter_cols"][-2]
        filter_values = None
    if isinstance(filter_values, str):
        if filter_values == "":
            filter_values = None
        else:
            parsed_list_codes = filter_values.strip().split(",")
            parsed_list_codes = [code.strip() for code in parsed_list_codes]
            filter_values = parsed_list_codes
    query_warning = ""
    if filter_values != None and len(filter_values) > 1:
        query_warning_msg = [
            "⚠ Si l'on sélectionne sur plusieurs codes simultanément, le nombre de bénéficiaires affiché est la somme du nombre de bénéficiaires pour chacun des codes, ce qui est une borne haute du nombre de bénéficiaires réel. En effet, si un même bénéficiaire a consommé des prestations pour 2 codes, il sera comptabilisé 2 fois.",
        ]
        query_warning = dbc.Alert(query_warning_msg, color="warning")
    nb_warning = ""
    popover_warning_children = [
        dbc.PopoverBody(
            [
                html.P(
                    """
            Il existe ensuite plusieurs correspondances d'un code CIP vers les codes ATC.
            Exemple : le paracétamol codéiné (CIP 34009 332 758 1 5) peut être relié à l'ATC N02AA59 ou ATC N02AJ06
        """
                )
            ],
            style={"text-align": "left"},
        )
    ]
    popover_warning = dbc.Popover(
        children=popover_warning_children,
        id="popover-warning-opensnds",
        target="warning-opensnds",
        trigger="hover",
    )
    if filter_col.startswith("ATC"):
        nb_warning_msg = [
            "Attention à l'usage des codes ATC. Les lignes de prestations de pharmacie sont enregistrées par code CIP dans le SNDS.",
            html.Span(html.B(" ⓘ"), id="warning-opensnds"),
        ]
        nb_warning = [dbc.Alert(nb_warning_msg, color="warning"), popover_warning]
    nomenclature_choices = return_nomenclature_choices(filter_col, ds_name, schemas)
    (
        figure_sex,
        figure_age,
        figure_reg,
        figure_time,
        figure_speprec,
        df_sex,
        df_age,
        df_reg,
        df_time,
        df_speprec,
    ) = dashboard_update_figure(filter_col, filter_values, ds_name, years)
    return [
        figure_sex,
        figure_age,
        figure_reg,
        figure_time,
        figure_speprec,
        df_sex,
        df_age,
        df_reg,
        df_time,
        df_speprec,
        ds_name,
        level_choices,
        nomenclature_choices,
        years_choices,
        years_choices,
        years_choices,
        years_choices,
        years,
        years,
        years,
        years,
        filter_col,
        filter_values,
        query_warning,
        nb_warning,
    ]


def dashboard_update_figure(filter_col, filter_values, ds_name, years):
    """Run the user query against the spark engine and update the 4 figures and data tables (time, sex, age, region)

    Args:
        n_clicks ([type]): [description]
        filter_col ([type]): [description]
        filter_values ([type]): [description]
        ds_name ([type]): [description]
        years ([type]): [description]

    Returns:
        [type]: [description]
    """
    sub_db_name = ds_name
    if SUPPORTED_DBS[ds_name]["consommants"]:
        sub_db_name = sub_db_name + "__" + filter_col.lower()
    payload = {}
    payload = {
        "ds_name": ds_name,
        "ds_schema": schemas[ds_name],
        "filter_col": filter_col,
        "filter_values": filter_values,
        "years": years,
    }
    logging.warning("Query with payload : {}".format(payload))
    payload["dataset"] = datasets[sub_db_name]
    figures_outputs = {}
    data_outputs = {}
    for output_category in ["sex", "age", "region", "spe_presc"]:
        payload["group_cols"] = SUPPORTED_DBS[ds_name]["focused_dimensions"][
            output_category
        ]
        fig, data = get_group_histogram(**payload)
        figures_outputs[output_category] = fig
        if filter_values is None:
            display_nb_consommants = 0
        else:
            display_nb_consommants = len(filter_values)
        data_outputs[output_category] = nice_dash_table(
            data, f"{output_category}_table", None, display_nb_consommants
        )
    # specific modifications
    figures_outputs["region"] = figures_outputs["region"].update_layout(
        xaxis=go.layout.XAxis(tickangle=45)
    )
    payload.pop("years")
    payload["group_cols"] = SUPPORTED_DBS[ds_name]["frequency"][0]
    fig, data = get_time_barplot(**payload)
    figures_outputs["time"] = fig
    if filter_values is None:
        display_nb_consommants = 0
    else:
        display_nb_consommants = len(filter_values)
    data_outputs["time"] = nice_dash_table(
        data, f"{output_category}_table", None, display_nb_consommants
    )
    return (
        figures_outputs["sex"],
        figures_outputs["age"],
        figures_outputs["region"],
        figures_outputs["time"],
        figures_outputs["spe_presc"],
        data_outputs["sex"],
        data_outputs["age"],
        data_outputs["region"],
        data_outputs["time"],
        data_outputs["spe_presc"],
    )


@app.callback(
    Output("dashboard-clipboard-sql", "children"),
    [
        Input("click-query-sql", "n_clicks"),
        Input("dashboard-level-choices", "value"),
        Input("dashboard-filter-values", "value"),
        Input("input-dataset-name", "value"),
    ],
)
def dashboard_query_sql(n_clicks, terminologie, filter_values, ds_name):
    return [
        dbc.PopoverHeader("Requête SQL à passer dans SAS"),
        dbc.PopoverBody(
            encapsSAS(
                ds_name,
                tradSQL(
                    ds_name,
                    None,
                    filter_values,
                    [("ER_PRS_F", "EXE_SOI_DTD", "year")],
                    None,
                    terminologie,
                ),
            )
        ),
    ]


@app.callback(
    Output("dashboard-clipboard-sql-sex", "children"),
    [
        Input("click-query-sql-sex", "n_clicks"),
        Input("input-dataset-name", "value"),
        Input("dashboard-level-choices", "value"),
        Input("dashboard-filter-values", "value"),
        Input("dashboard-year-choices-sex", "value"),
    ],
)
def dashboard_query_sql_sex(n_clicks, ds_name, terminologie, filter_values, years):
    return [
        dbc.PopoverHeader("Requête SQL à passer dans SAS"),
        dbc.PopoverBody(
            encapsSAS(
                ds_name,
                tradSQL(
                    ds_name,
                    None,
                    filter_values,
                    [("IR_BEN_R", "BEN_SEX_COD")],
                    years,
                    terminologie,
                ),
            )
        ),
    ]


@app.callback(
    Output("dashboard-clipboard-sql-age", "children"),
    [
        Input("click-query-sql-age", "n_clicks"),
        Input("input-dataset-name", "value"),
        Input("dashboard-level-choices", "value"),
        Input("dashboard-filter-values", "value"),
        Input("dashboard-year-choices-age", "value"),
    ],
)
def dashboard_query_sql_age(n_clicks, ds_name, terminologie, filter_values, years):
    return [
        dbc.PopoverHeader("Requête SQL à passer dans SAS"),
        dbc.PopoverBody(
            encapsSAS(
                ds_name,
                tradSQL(
                    ds_name,
                    None,
                    filter_values,
                    [("IR_BEN_R", "BEN_NAI_ANN")],
                    years,
                    terminologie,
                ),
            )
        ),
    ]


@app.callback(
    Output("dashboard-clipboard-sql-reg", "children"),
    [
        Input("click-query-sql-sex", "n_clicks"),
        Input("input-dataset-name", "value"),
        Input("dashboard-level-choices", "value"),
        Input("dashboard-filter-values", "value"),
        Input("dashboard-year-choices-reg", "value"),
    ],
)
def dashboard_query_sql_reg(n_clicks, ds_name, terminologie, filter_values, years):
    return [
        dbc.PopoverHeader("Requête SQL à passer dans SAS"),
        dbc.PopoverBody(
            encapsSAS(
                ds_name,
                tradSQL(
                    ds_name,
                    None,
                    filter_values,
                    [("IR_BEN_R", "BEN_RES_DPT")],
                    years,
                    terminologie,
                ),
            )
        ),
    ]


@app.callback(
    Output("collapse-complex-query", "is_open"),
    [Input("collapse-button-complex-query", "n_clicks")],
    [State("collapse-complex-query", "is_open")],
)
def toggle_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


def to_xlsx(bytes_io, df, ds_name, level, codes, year=None):
    """Binary writer for xlsx download"""
    xslx_writer = pd.ExcelWriter(bytes_io, engine="xlsxwriter")
    date_export = datetime.datetime.now()
    date_export_string = date_export.strftime("%d/%m/%Y")
    metadata = {}
    metadata["Jeu de donnée"] = ds_name
    metadata["Terminologie"] = level
    if isinstance(codes, str):
        metadata["Codes"] = codes
    elif isinstance(codes, list):
        # Pour les codes NABM, Dash renvoie des int
        string_codes = [str(code) for code in codes]
        metadata["Codes"] = ", ".join(string_codes)
    else:
        metadata["Codes"] = "Pas de codes sélectionnées"
    if year is not None:
        metadata["Année"] = year
    metadata["date_export"] = date_export_string
    pd.DataFrame.from_records(df["props"]["data"]).to_excel(
        xslx_writer, index=False, sheet_name="Donnees"
    )
    pd.DataFrame.from_records(metadata, index=[0]).to_excel(
        xslx_writer, index=False, sheet_name="MetaDonnees"
    )
    xslx_writer.save()


@app.callback(
    Output("download-year", "data"),
    [Input("export-year", "n_clicks")],
    [
        State("output-time-df", "children"),
        State("input-dataset-name", "value"),
        State("dashboard-level-choices", "value"),
        State("dashboard-search-input-list-codes", "value"),
    ],
)
def export_year_xlsx(n_clicks, df, ds_name, level, codes):
    if df is not None:
        to_xlsx_args = {"df": df, "ds_name": ds_name, "level": level, "codes": codes}
        return send_bytes(to_xlsx, f"opensnds_{ds_name}_par_annee.xlsx", **to_xlsx_args)
    return None


@app.callback(
    Output("download-sex", "data"),
    [Input("export-sex", "n_clicks")],
    [
        State("output-sex-df", "children"),
        State("input-dataset-name", "value"),
        State("dashboard-level-choices", "value"),
        State("dashboard-search-input-list-codes", "value"),
        State("dashboard-year-choices-sex", "value"),
    ],
)
def export_sex_xlsx(n_clicks, df, ds_name, level, codes, year):
    if df is not None:
        to_xlsx_args = {
            "df": df,
            "ds_name": ds_name,
            "level": level,
            "codes": codes,
            "year": year,
        }
        return send_bytes(to_xlsx, f"opensnds_{ds_name}_par_sexe.xlsx", **to_xlsx_args)
    return None


@app.callback(
    Output("download-age", "data"),
    [Input("export-age", "n_clicks")],
    [
        State("output-age-df", "children"),
        State("input-dataset-name", "value"),
        State("dashboard-level-choices", "value"),
        State("dashboard-search-input-list-codes", "value"),
        State("dashboard-year-choices-age", "value"),
    ],
)
def export_age_xlsx(n_clicks, df, ds_name, level, codes, year):
    if df is not None:
        to_xlsx_args = {
            "df": df,
            "ds_name": ds_name,
            "level": level,
            "codes": codes,
            "year": year,
        }
        return send_bytes(to_xlsx, f"opensnds_{ds_name}_par_sexe.xlsx", **to_xlsx_args)
    return None


@app.callback(
    Output("download-reg", "data"),
    [Input("export-reg", "n_clicks")],
    [
        State("output-reg-df", "children"),
        State("input-dataset-name", "value"),
        State("dashboard-level-choices", "value"),
        State("dashboard-search-input-list-codes", "value"),
        State("dashboard-year-choices-reg", "value"),
    ],
)
def export_reg_xlsx(n_clicks, df, ds_name, level, codes, year):
    if df is not None:
        to_xlsx_args = {
            "df": df,
            "ds_name": ds_name,
            "level": level,
            "codes": codes,
            "year": year,
        }
        return send_bytes(to_xlsx, f"opensnds_{ds_name}_par_sexe.xlsx", **to_xlsx_args)
    return None


@app.callback(
    Output("download-speprec", "data"),
    [Input("export-speprec", "n_clicks")],
    [
        State("output-speprec-df", "children"),
        State("input-dataset-name", "value"),
        State("dashboard-level-choices", "value"),
        State("dashboard-search-input-list-codes", "value"),
        State("dashboard-year-choices-speprec", "value"),
    ],
)
def export_speprec_xlsx(n_clicks, df, ds_name, level, codes, year):
    if df is not None:
        to_xlsx_args = {
            "df": df,
            "ds_name": ds_name,
            "level": level,
            "codes": codes,
            "year": year,
        }
        return send_bytes(
            to_xlsx,
            f"opensnds_{ds_name}_par_specialite_prescripteur.xlsx",
            **to_xlsx_args,
        )
    return None


def start_dev_server():
    app.run_server(debug=(DEBUG_APP == "True"), host="0.0.0.0")


if __name__ == "__main__":
    start_dev_server()
