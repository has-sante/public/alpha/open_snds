#!/usr/bin/env python

from opensnds.data.compile import (
    process_dataset,
    download_schemas,
    download_nomenclatures,
)
from pathlib import Path
from opensnds.constants import (
    DIR2SCHEMAS,
    DIR2RAW,
    SUPPORTED_EXTENSIONS,
    DIR2INTERIM,
)
from opensnds.config import SUPPORTED_DBS
import os
import click
import json
import re
import pandas as pd


@click.command()
@click.option(
    "--dbs",
    "-dbs",
    type=str,
    multiple=True,
    help="List of names of databases to process",
    default=["openmedic", "openbio"],
)
@click.option("--dl_schemas", type=bool, default=True)
@click.option("--dl_nomenclatures", type=bool, default=True)
@click.option("--clean_db", type=bool, default=False)
def compile(dbs, dl_schemas, dl_nomenclatures, clean_db):
    """
    Compiles the data and partition by frequency
    """
    print(dbs, dl_schemas, dl_nomenclatures, clean_db)
    if dl_schemas:
        download_schemas()
    if dl_nomenclatures:
        # Nomenclatures (verify doit être forcé à False sur le réseau HAS)
        download_nomenclatures(verify=False)
    for db in dbs:
        process_dataset(db, clean_db)
    return


def checkCSV(db_name="openmedic"):
    schema = json.load(open(Path(DIR2SCHEMAS, db_name + ".json")))
    setField = set()
    for field in schema["fields"]:
        setField.add(field["name"].upper())
    print(setField)
    config = SUPPORTED_DBS[db_name]
    if config["consommants"]:
        source_dirs = [
            str(DIR2RAW / db_name / lvl.lower()) for lvl in config["level_choices"]
        ]
    else:
        source_dirs = [str(DIR2RAW / db_name)]
    for source_dir in source_dirs:
        dataset_dir = DIR2RAW / source_dir
        files_to_process = sorted(os.listdir(dataset_dir))
        for filename in files_to_process:
            path2file = Path(dataset_dir, filename)
            if re.search("|".join(SUPPORTED_EXTENSIONS), filename):
                try:
                    print(path2file)
                    df_pd = pd.read_csv(
                        path2file, delimiter=";", encoding="latin-1", engine="python"
                    )
                    df_pd.columns = [x.upper() for x in df_pd.columns]
                    df_pd.info()
                    columnsCSV = df_pd.columns
                    setColumns = set(columnsCSV)
                    print("Difference Shema")
                    print(setField.difference(setColumns))
                    ##print(df_pd.groupby(["AGE"]).agg(["count"]))
                except UnicodeDecodeError as err:
                    print("Unicode Decode error: {0}".format(err))


def checkParquet(db_name="openmedic"):
    schema = json.load(open(Path(DIR2SCHEMAS, db_name + ".json")))
    setField = set()
    for field in schema["fields"]:
        setField.add(field["name"].upper())
    dataset = pd.read_parquet(str(DIR2INTERIM / db_name / "atc1"), engine="pyarrow")
    print(dataset)
    dataset.columns = [x.upper() for x in dataset.columns]
    dataset.info()
    columnsCSV = dataset.columns
    setColumns = set(columnsCSV)
    print("Difference Shema")
    print(setField.difference(setColumns))


def checkParquetByCode(db_name="openbio"):
    schema = json.load(open(Path(DIR2SCHEMAS, db_name + ".json")))
    setField = set()
    for field in schema["fields"]:
        setField.add(field["name"].upper())
    dataset = pd.read_parquet(
        str(Path(DIR2INTERIM / db_name / "acte")), engine="pyarrow"
    )
    print(dataset)
    dataset.columns = [x.upper() for x in dataset.columns]
    dataset.info()
    print(dataset.query("ACTE == 1488").groupby("YEAR").size())


def checkCSVByCode(db_name="openbio", codes=["1488"]):
    df_pd = pd.read_csv(
        "/opt/open_snds/data/raw/openbio/acte/NB_2017_ACTE_age_sexe_reg_spe.CSV.gz",
        delimiter=";",
        encoding="latin-1",
        engine="python",
    )
    print(df_pd)
    df_pd.info()
    print(df_pd.query("BIO_PRS_IDE == 1488"))


if __name__ == "__main__":
    compile()
