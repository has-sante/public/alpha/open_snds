# %%
import pyspark.sql.functions as F
from opensnds.preprocessing.schema_handler import load_schemas
from opensnds.app.utils.back_utils import (
    load_datasets,
    query_table,
    get_group_histogram,
)
from pathlib import Path
import logging
from IPython import get_ipython

# from opensnds.utils import sc
from opensnds.constants import *
from opensnds.config import SUPPORTED_DBS
import os
import re
import pandas as pd
from opensnds.data.compile import *

logging.basicConfig(level=logging.INFO)
get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")

# %%
ds_name = "openlpp"
config = SUPPORTED_DBS[ds_name]
datasets = load_datasets()
schemas = load_schemas()
print(config)


# %%
filter_col = "CODE_LPP"
sub_db_name = ds_name
filter_values = ["3518"]
years = "2019"
if SUPPORTED_DBS[ds_name]["consommants"]:
    sub_db_name = sub_db_name + "__" + filter_col.lower()
payload = {
    "ds_name": ds_name,
    "ds_schema": schemas[ds_name],
    "dataset": datasets[sub_db_name],
    "filter_col": filter_col,
    "filter_values": filter_values,
    "years": years,
}
payload["group_cols"] = SUPPORTED_DBS[ds_name]["focused_dimensions"]["sex"]

# %%
fig, df = get_group_histogram(**payload)

# %%
df = query_table(**payload).set_index(VALUE_COLS).reset_index()
df

# %%
datasets["openlpp__code_lpp"]["year"].drop_duplicates().values

# %%
db_14 = sc.read.csv(
    os.path.join(DIR2RAW_OPENLPP, "OPEN_LPP_2019.csv"), sep=";", header=True
)

db_19 = sc.read.csv(
    os.path.join(DIR2RAW_OPENLPP, "OPEN_LPP_2014.csv"), sep=";", header=True
)
db_19 = clean_montants(db_19)
# %%

# %%
db_14 = db_14.withColumn("REM", F.col("REM").cast("double"))

# %%
db_14.dtypes

# %%
db_14.filter(F.col("REM").isNull()).show()


# %%
db_19.filter(F.col("REM").isNull()).show()


# %%
#  Create test data for the app
for db, conf in SUPPORTED_DBS.items():
    dataset_dir = conf["directory"]
    files_to_process = sorted(os.listdir(dataset_dir))
    test_file = files_to_process[0]
    df = pd.read_csv(os.path.join(dataset_dir, test_file), nrows=10000, sep=";")
    df.to_parquet(os.path.join(DIR2TEST_DATA, "interim", f"{db}.parquet"), index=False)

# %%
