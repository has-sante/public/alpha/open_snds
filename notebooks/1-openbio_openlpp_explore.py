# %%
import logging
from IPython import get_ipython
from opensnds.constants import *
import os
import pandas as pd
from opensnds.preprocessing.schema_handler import *

logging.basicConfig(level=logging.INFO)
get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")
# %%
dbschemas = get_schemas()
# %%
dbname = "openbio"
dbschema = dbschemas[dbname]
openbio = pd.read_parquet(DIR2CLEAN_OPENBIO)
print(openbio.shape)
openbio.head(2)
# %%
col_to_get = ["GRP", "PSP_SPE", "SEXE", "ACTE", "AGE", "BEN_REG"]
join_nomenclatures(openbio, col_to_get, dbschema)
# %%
dbname = "openlpp"
dbschema = dbschemas[dbname]
openlpp = pd.read_parquet(DIR2CLEAN_OPENLPP)
print(openlpp.shape)
print(openlpp.dtypes)
openlpp.head(2)

# %%
pd.DataFrame(
    join_nomenclatures(openlpp, ["PSP_SPE"], dbschema, False)[
        ["PSP_SPE", "PSP_SPE_LIB", "PSP_SPE_SNDS"]
    ].value_counts()
).sort_values("PSP_SPE")
# %%
