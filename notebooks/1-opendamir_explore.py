# %%
from tableschema.schema import Schema
from IPython import get_ipython
from opensnds.constants import DIR2CLEAN_OPENDAMIR, DIR2INTERIM, DIR2TEST_OPENDAMIR
from opensnds.data.clean_opendamir import *
from opensnds.preprocessing.schema_handler import *
from opensnds.utils import sc
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
import logging

pd.set_option("display.max_columns", 80)
logger = logging.basicConfig(level=logging.INFO)
get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")

# %%
# download_nomenclatures()
dsname = "opendamir"
year = 2019
response_maxsize = 100000
opendamir = sc.read.parquet(DIR2CLEAN_OPENDAMIR)
# table = opendamir.limit(response_maxsize).toPandas()
# opendamir = pd.read_parquet(DIR2CLEAN_OPENDAMIR)
schemas = get_schemas()
ds_schema = schemas[dsname]
opendamir.head()
# %%
opendamir.groupby("PSE_SPE_SNDS").count().toPandas()

# %%
group_cols = [PRS_NAT, PSE_ACT_CAT]
resp = query_table(group_cols, opendamir, schemas[dsname])
resp["table"]
# %%
# Prescription vs. Executant
category_pse_psp = opendamir[["PSE_ACT_CAT", "PSP_ACT_CAT"]].copy()
category_pse_psp["count"] = 1
category_pse_psp = category_pse_psp.groupby(
    ["PSE_ACT_CAT", "PSP_ACT_CAT"], as_index=False
)["count"].sum()
category_pse_psp.pivot(index="PSP_ACT_CAT", columns="PSE_ACT_CAT", values="count")

# %%
opendamir.loc[opendamir["PSE_ACT_CAT"] == 2]["PRS_NAT"].value_counts()
# %%
prs_counts = (
    opendamir.groupby(["PRS_NAT"], as_index=False)["PRS_ACT_QTE"]
    .sum()
    .sort_values("PRS_ACT_QTE", ascending=False)
)
plt.bar(x=np.arange(len(prs_counts)), height=prs_counts["PRS_ACT_QTE"])
# %%
