# %%
from opensnds.preprocessing.schema_handler import add_missing_nomenclature_from_doc
import io
import requests
from opensnds.constants import *
from opensnds.config import *
from opensnds.preprocessing.schema_handler import *
from os import error
import pandas as pd
import json
import logging
from IPython import get_ipython

logger = logging.basicConfig(level=logging.INFO)

get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")
# %%
test_gitalb = pd.read_csv(
    "https://gitlab.com/strayMat/parietools/-/blob/master/test.csv"
)

# %%
openlpp = pd.read_parquet(DIR2INTERIM_OPENLPP)
openlpp.shape
openlpp.head()

# %%
openbio = pd.read_parquet(DIR2INTERIM_OPENBIO)
openbio.shape
openbio.head()

# %%
openmedic = sc.read.parquet(DIR2INTERIM_OPENMEDIC)
print(openmedic.count())
openmedic_header = openmedic.limit(10).toPandas()
openmedic_header.head()

# %%
dbname = "openmedic"
path2documentation = os.path.join(ROOT_DIR, "documentation", f"Lexique_{dbname}.xls")

# %%
df = pd.read_excel(path2documentation, sheet_name=None)


# %%
add_missing_nomenclature_from_doc(dbname)

# %%
s = load_schema(dbname)

# %%
