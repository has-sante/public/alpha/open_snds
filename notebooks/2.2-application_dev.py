from opensnds.app.constants import *
from opensnds.constants import *
from opensnds.app.utils.back_utils import (
    query_table,
    load_datasets,
    get_group_histogram,
    get_time_barplot,
)
from opensnds.preprocessing.schema_handler import load_schemas
from opensnds.config import SUPPORTED_DBS
from opensnds.preprocessing.schema_handler import *
import databricks.koalas as ks

pd.set_option("display.max_columns", 500)
# from opensnds.utils import sc
# %load_ext autoreload
# %autoreload 2

DEBUG = False
datasets = load_datasets(DEBUG)
schemas = load_schemas()

df = datasets["openbio__acte"]

df["year"].value_counts()

df.query("year == 2019").groupby("ACTE").count()

lfiles = DIR2RAW / "openbio" / "acte"

os.listdir(lfiles)

df_raw_15 = pd.read_csv(
    lfiles / "NB_2015_ACTE_age_sexe_reg_spe.CSV.gz",
    compression="gzip",
    encoding="latin1",
    sep=";",
)
print(df_raw_15.dtypes)
df_raw_15.head()

df_raw_19 = pd.read_csv(
    lfiles / "NB_2019_ACTE_age_sexe_reg_spe.CSV.gz",
    compression="gzip",
    encoding="latin1",
    sep=";",
)
print(df_raw_19.dtypes)
df_raw_19.head()


data_information
