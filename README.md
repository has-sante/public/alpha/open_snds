# À propos du projet

[Open SNDS](https://open-snds.has-sante.fr/) est une application web pour interroger rapidement certaines données agrégées du SNDS, publiées en open data par l'Assurance maladie.

On interroge les données en choisissant une base (Open Medic, Open Bio, éventuellement d'autres à venir), puis en filtrant sur un (ou plusieurs) codes,  aux différents niveaux de hierarchie des nomenclatures. 

On visualise alors le nombre de bénéficiaire, le nombre de consommation, et les montant de remboursements. 
Ces informations sont déclinés selon plusieurs axes d'analyse : évolution annuelle depuis 2014, répartition selon les caractéristiques du bénéficiaire (sexe, classe d'âge, région), ou la spécialité du prescripteur.
Elles sont affichées via un graphique, et un tableau qu'il est possible de télécharger.

L'application Open SNDS est développée et hébergée par la Haute Autorité de Santé.

Liens :
- l'application [open-snds.has-sante.fr/](https://open-snds.has-sante.fr/)
- le code [gitlab.com/has-sante/public/open_snds](https://gitlab.com/has-sante/public/open_snds), diffusé [sous licence EUPL](LICENCE).
- la documentation en ligne [has-sante.gitlab.io/public/open_snds/](https://has-sante.gitlab.io/public/open_snds/)

![capture](capture.png)

# Installation

Les différentes étapes de l'installation du projet sont expliquées en détail sur les fichier INSTALL.md.

## Pré-requis

- podman
- podman-compose
- python 3.8
- poetry

## Sources de données

La description des sources de données utilisées dans l'application sont disponibles à l'url : [https://open-snds.has-sante.fr/dataapp](https://open-snds.has-sante.fr/dataapp)
Les données utilisées par l'application peuvent être stockées soit en local soit sur un storage object MinIO. Les données sont ensuite lues et chargés en mémoire comme dataframe pandas.

## Configuration du projet

### Récupération du code

Récupération du code depuis gitlab :

    git clone https://gitlab.com/has-sante/public/open_snds.git
    cd open_snds

### Configuration des variables d'environment

Copie du template vers `.env`:

    cp template.env .env  # Crée un fichier de configuration

Remplacer les variables dans le `.env`.

### Suivi de l'activité 

Un suivi de l'activité est réalisée sur l'application Open SNDS pour suivre l'activité et le trafic sur celle-ci. 

### Mise en place technique

Ce suivi se fait sans cookies.

Un fichier `smarttag.js` est généré depuis le site Piano Analytics est intégré au front-end de l'application (dans le dossier `assets`), puis le tag est initialisé pour suivre l'activité à la connexion de l'utilisateur.

### Accès et informations disponibles

L'accès se fait en se rendant sur le site de [Piano Analytics](https://explorer.atinternet-solutions.com/) et en se connectant avec le compte de la mission data (data@has-sante.fr), puis en sélectionnant le site `Open SNDS`

Les informations disponibles sont les suivantes :
- Nombre de visites quotidiennes
- Temps moyen passé par visite
- Informations techniques sur l'utilisateur (OS, navigateur, résolution d'écran)
