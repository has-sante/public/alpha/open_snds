# Pbs techniques
- Dowloads : Nous devrions avoir un accès en URL à ces données, même avec une clé de téléchargement.

- Requêtages : aggregation à la volée : Oui mais ne changerait pas grand chose avec spark en réalité. Il faut seulement savoir quel référentiel est lié à quelle donnée

- Format des nomenclatures : csv avec separateurs mixtes, très irritant, pourrait être corrigé à la source.

# TODO

- menu de selection de la base :
- Faire dashboard avec pour chaque code à la volée: résultats temporels, sexe, age, type etb, spécialité ?  
- Intégration openbio, opensnds (depuis le site de l'atih) : code pr preprocessing OK, TODO: complétion des schemas avec les nomenclatures.
- il faut renommer les colonnes qui ont une nomenclature mais pas le bon nom de colonne.
- créer les nomenclatures ATC à partir de IR_PHA_R


Référentiels communs d'un jeu de données à l'autre :
- region
- dpt
- datetime column
- age
- sexe
- PSP_SPE


# Référentiels supplémentaires

- rpps : [lien drees](https://data.drees.solidarites-sante.gouv.fr/explore/dataset/514_la-demographie-des-medecins-rpps-au-1er-janvier/information/)

- données socio-démo : insee

- limites administratives des départements/régions pour la cartographie : [lien api geo](https://api.gouv.fr/les-api/api-geo)

- APL : [lien drees](https://data.drees.solidarites-sante.gouv.fr/explore/dataset/530_l-accessibilite-potentielle-localisee-apl/information/)
# Usages

- Qui prescrit à qui ?

- Nombre de consultations par prestation, spé, région, maille temporelle : la maille géographique peut-être le département si l'on regarde le fichier R (mais dans ce cas pas d'infos sur les hospitalisations, présentes uniquement dans les données opendamir et non fichiers R).

- Rbts par trânche d'âge

- Les médicaments/prestations les plus remboursés, les plus effectués

- comment évoluent les restes à charge du patient en fonction du régime d’exonération ?

- Lien offre/demande : [résultats détaillés](https://github.com/SGMAP-AGD/densite_medecins)

## Régionalisation / Départementalisation

Y a t-il des profils spécifiques par région ?

Peut-on segmenter la consommation de médicaments par région ? (ex. prédire région selon le profil de prescription des médicaments ou des actes). Quelles sont les principales différences de prescriptions ? En terme de médicaments, en termes de

Ensuite on peut poser la question de manière plus fine par département/mois ou par exemple (avec open_ville).

Pb, peu de répétitions temporelles (année pr openmédic et mois pr les deux autres).

## Qualification des exécutants ou prescripteurs

Est-ce que des profils locaux de prescriptions-exécution existent ?  ex. par dpt ? Encore une fois peu de répétition.

# Inspirations
## All-OF-US project
Le all-of-Us project est le rassemblement de données EHR + suivi patient pr 1m d'américains. Les données sont basées sur le cloud et rendues accessibles aux chercheurs (procédure d'accès à voir). Des dashboards interactifs et actualisés sont dispos sur des indicateurs aggrégés ici : https://databrowser.researchallofus.org/

- un truc bof sur openccam en open: https://public.opendatasoft.com/explore/dataset/open_ccam/analyze/?flg=fr&disjunctive.libelle_regroupement&disjunctive.cat_etablissement&refine.annee=2017&location=6,47.16481,2.80838&basemap=jawg.streets&dataChart=eyJxdWVyaWVzIjpbeyJjaGFydHMiOlt7InR5cGUiOiJjb2x1bW4iLCJmdW5jIjoiQVZHIiwieUF4aXMiOiJuYl9hY3RlcyIsInNjaWVudGlmaWNEaXNwbGF5Ijp0cnVlLCJjb2xvciI6InJhbmdlLWN1c3RvbSJ9XSwieEF4aXMiOiJhbm5lZSIsIm1heHBvaW50cyI6IiIsInRpbWVzY2FsZSI6InllYXIiLCJzb3J0IjoiIiwic2VyaWVzQnJlYWtkb3duIjoibGliZWxsZSIsInN0YWNrZWQiOiJub3JtYWwiLCJjb25maWciOnsiZGF0YXNldCI6Im9wZW5fY2NhbSIsIm9wdGlvbnMiOnsiZmxnIjoiZnIiLCJkaXNqdW5jdGl2ZS5saWJlbGxlX3JlZ3JvdXBlbWVudCI6dHJ1ZSwiZGlzanVuY3RpdmUuY2F0X2V0YWJsaXNzZW1lbnQiOnRydWUsInJlZmluZS5hbm5lZSI6IjIwMTcifX19XSwiZGlzcGxheUxlZ2VuZCI6dHJ1ZSwiYWxpZ25Nb250aCI6dHJ1ZX0%3D
